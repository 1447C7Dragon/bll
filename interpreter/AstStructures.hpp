#ifndef PROGRAMSTRUCTURES_H
#define PROGRAMSTRUCTURES_H
/** \file
 * Plik zawiera deklaracje typów danych używanych w drzewie składniowym (wyniku kompilacji programu)
 * oraz funkcji do operowania nimi.
 */

#include <iostream>


namespace Interpreter
{
namespace AST
{

/**
 * Operacje wykonywane w kolejnych węzłach działania zapisanego w RPN.
 */
enum class Operator : unsigned char
{
    ///Odłóż wartość na stosie
    PUSH_VAL,

    //Odłóż wartość zmiennej na stosie
    PUSH_VAR,

    ///Zdejmij dwie wartości ze stosu dodaj je i umieść wynik na stosie
    ADD,

    ///Zdejmij dwie wartośći ze stosu, odejmij pierwszą od drugiej i wynik umieść na stosie
    SUBTRACT,

    ///Zdejmij dwie wartosci ze stosu, pomnóż je i wynik umieść na stosie
    MULTIPLY,

    //Zdejmij dwie wartości ze stosu, podziel drugą przez pierwszą i wynk umieść na stosie
    DIVIDE
};

/**
 * Pojedyncza instrukcja w działaniu zapisanym w RPN.
 */
struct RpnNode
{
    ///Operacja do wykonania
    Operator op;

    ///Dodatkowe dane potrzebne do wykonania operacji:
    /// - Wartość <c>double</c>, gdy <c>op == Operator::PUSH_VAL</c>
    /// - Struktura <c>VariableAddress</c>, gdy <c>op == Operator::PUSH_VAR</c>
    /// - <c>nullptr</c> w innym wypadku
    void *arg = nullptr;

    ///Wskaźnik na kolejną instrukcję w wyrażeniu albo <c>nullptr</c> gdy ta jest ostatnią
    RpnNode *next = nullptr;
};

// struct Variable
// {
//     enum Type : unsigned char
//     {
//         BYTE,
//         INTEGER,
//         LONG_INTEGER,
//         SINGLE,
//         DOUBLE
//     };
//
//     Type type;
//     void *data = nullptr;
// };

/**
 * Adres zmiennej w rejestrze zmiennych.
 * \see Interpreter::Program::_register
 */
struct VariableAddress
{
    ///Numer rejestru, w którym umieszczona jest zmienna
    unsigned int regnum;

    ///Numer zmiennej w rejestrze <c>regnum</c>
    unsigned int num;
};

/**
 * Reprezentacja zmiennej.
 */
struct Variable
{
    typedef unsigned char TByte;
    typedef long int TInteger;
    typedef double TDouble;

    enum Type : unsigned char
    {
        BYTE,
        INTEGER,
        DOUBLE,
    };

    Type type;
    void *ptr;
};

/**
 * Bloki z instrukcjami w drzewie składniowym.
 * \bug Spacjalizacja?
 */
struct Node
{
    /**
     * Jaka instrukcja ma byc wykonana w tym bloku.
     */
    enum Instruction : unsigned char
    {
        ///Zatrzymaj program
        HALT,

        ///Utwórz nowy rejestr. Używane przy wchodzeniu do kontekstu ze zmiennymi
        PUSH_CONTEXT,

        ///Usuń rejestr. Wykonywane przy opuszczaniu kontekstu ze zmiennymi
        POP_CONTEXT,

        ///Przypisz zmiennej wartość wyrażenia zapisanego w RPN
        ASSIGN,

        ///Wypisz zmienną na <c>stdout</c>
        OUTPUT,
    };

    ///Instrukcja zapisana w bloku
    Instruction inst;

    ///Dodatkowe dane potrzebne do wykonania operacji:
    /// - jeśli <c>inst == Instruction::PUSH_CONTEXT</c>, to wskazuje na strukturę <c>Interpreter::AST::PushContextData</c>
    /// - jeśli <c>inst == Instruction::POP_CONTEXT</c>, to wskazuje na liczbę <c>unsigned int</c> -- numer rejestru do usunięcia
    /// - jeśli <c>inst == Instruction::ASSIGN</c>, to wskazuje na strukturę <c>Interpreter::AST::AssignementData</c>
    ///\see Interpreter::AST::PushContextData
    ///\see Interpreter::AST::AssignementData
    void *data = nullptr;

    ///Wskaźnik na kolejną instrukcję w drzewie – ona powinna być wykonana jako następna. <c>nullptr</c> oznacza, że wszystkie
    ///instrukcje w danym bloku zostały wykonane.
    Node *next = nullptr;
};

/**
 * Dane potrzebne do utworzenia oraz znieszczenia nowego rejestru.
 */
struct PushContextData
{
    ///Numer rejestru, który ma zostać przesłonięty przez nowy rejestr
    unsigned int regnum;

    ///Rozmiar nowego rejestru
    unsigned int size;

    ///Tablica z typami kolejnych zmiennych
    Variable::Type *skel = nullptr;
};

/**
 * Dane potrzebne do przypisania wartości do zmiennej
 */
struct AssignementData
{
    ///Położenie zmiennej w rejestrze
    VariableAddress address;

    ///Wyrażenie, którego wartość ma zostać przypisana
    RpnNode *expr = nullptr;
};


/**
 * Usuwa całe drzewo składniowe. Usuwa podany węzeł i przechodzi do tego, na który wskazywał, by wykonać te same operacje.
 * Dodatkowe dane przechpwane w węzłach drzewa takrze zostaną usunięte (również zagnieżdżone drzewa)
 * \param ast wskaźnik na pierwszy węzeł w drzewie
 */
void ast_delete(Node *ast);


/**
 * Iteruje się po wszystkich instrukcjach w wyrażeniu i usuwa je.
 * \param rpn wyrażenie do usunięcia
 */
void rpn_delete(AST::RpnNode *rpn);


#define STACK_PUSH(_VAL_) if (stack_size <= MAX_STACK_SIZE) {\
                              stack[stack_size] = _VAL_; \
                              ++stack_size; \
                          } else {\
                              std::cerr << "rpn_calculate:" << __FILE__ <<  ":" << __LINE__ << "> stack size exceeded!" << std::endl; \
                              abort();\
                          }
#define STACK_POP(_DEST_) if (stack_size > 0) {\
                              _DEST_ = stack[stack_size - 1]; \
                              --stack_size;\
                          } else { \
                              std::cerr << "rpn_calculate:" << __FILE__ <<  ":" << __LINE__ << "> stack empty!" << std::endl; \
                              abort(); \
                          }
template<class ReturnType> ReturnType rpn_value(RpnNode *expr, const Variable **vars)
{
    const unsigned int MAX_STACK_SIZE = 256;
    unsigned int stack_size = 0;
    ReturnType *stack = new ReturnType[MAX_STACK_SIZE];

    while (expr != nullptr) {
        if (expr->op == Operator::PUSH_VAL) {
            STACK_PUSH(*static_cast<ReturnType*>(expr->arg));
        } else if (expr->op == Operator::PUSH_VAR) {
            VariableAddress *index;
            index = static_cast<VariableAddress*>(expr->arg);
            ReturnType val;
            switch (vars[index->regnum][index->num].type) {
                case Variable::BYTE:
                    val = static_cast<ReturnType>(*static_cast<Variable::TByte*>(vars[index->regnum][index->num].ptr));
                    break;
                case Variable::INTEGER:
                    val = static_cast<ReturnType>(*static_cast<Variable::TInteger*>(vars[index->regnum][index->num].ptr));
                    break;
                case Variable::DOUBLE:
                    val = static_cast<ReturnType>(*static_cast<Variable::TDouble*>(vars[index->regnum][index->num].ptr));
                    break;
            }
            STACK_PUSH(val);
        } else {
            ReturnType arg1, arg2;
            STACK_POP(arg1);
            STACK_POP(arg2);
            switch (expr->op) {
                case Operator::ADD:
                    STACK_PUSH(arg1 + arg2);
                    break;
                case Operator::SUBTRACT:
                    STACK_PUSH(arg2 - arg1);
                    break;
                case Operator::MULTIPLY:
                    STACK_PUSH(arg1 * arg2);
                    break;
                case Operator::DIVIDE:
                    STACK_PUSH(arg2 / arg1);
                    break;
            }
        }

        expr = expr->next;
    }

    if (stack_size > 1)
        std::cerr << "rpn_calculate:" << __FILE__ <<  ":" << __LINE__ << "> warning> stack not empty" << std::endl;
    ReturnType result = *stack;
    delete [] stack;
    return result;
}
#undef STACK_PUSH
#undef STACK_POP

}
}

#endif // PROGRAMSTRUCTURES_H
