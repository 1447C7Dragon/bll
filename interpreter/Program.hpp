#ifndef INTERPRETER_PROGRAM_HPP
#define INTERPRETER_PROGRAM_HPP
/** \file
 * Plik zawiera deklarację dkasy <c>Interpreter::Program</c> oraz powiązane z nim klasy i struktury. Jest to główny plik przestrzeni nazw
 * <c>Interpreter</c>.
 */

#include <string>
#include <stack>

#include "LexicalScaner.hpp"
#include "AstStructures.hpp"
#include "VariablesManager.hpp"

/** \namespace
 * Przestrzeń nazw zawierajaca wszystko związane z interpreterem języka.
 */
namespace Interpreter {

/**
 * Wynik kompilacji pliku. W przypadku błędu znajdują się tam informacje o jego kodzie, miejscu wystąpienia oraz komentarzu.
 */
struct ParseInfo
{
    /**
     * Wyliczenie z kodami błędów kompilacji.
     */
    enum Error : unsigned char
    {
        ///Kompilacja udana, nie znaleziono błędów.
        NO_ERROR,

        ///Oczekiwano poprawnej nazwy ([a-z][a-z0-9_]*)
        NAME_EXPECTED,

        ///Zmienna/funkcja/cokolwiek-innego-o-tej-nazwie zostało już zadeklarowane
        NAME_REDEFINED,

        ///Nieoczekiwany koniec pliku
        UNEXPECTED_EOF,

        ///Błąd wejścia/wyjścia (plik nie otwarty?)
        IO_ERROR,

        ///Coś innego miało byc w tym miejscu
        UNEXPECTED_TOKEN,

        ///Odwołanie do niezadeklarowanej funkcji/zmiennej
        UNKNOWN_NAME,
    };


    ///Numer linii w pliku, w której znajdował się błąd
    int line = -1;

    ///Ostatniego przeczytanego znaku w danej linii
    int column = -1;

    ///Kod błędu
    Error error = ParseInfo::NO_ERROR;

    ///Nazwa pliku
    std::string file;

    ///Komentarz kompilatora
    std::string message;
};



/**
 * Rzucany w przypadku wystąpienia błędu przy parsowaniu pliku. Przechwytywany wewnątrz klasy Program (nie wydostaje się na zewnątrz).
 * Jego wystąpienie sygnalizowane jest zwróceniem odpowiednich wartości w strukturze <c>ParseInfo</c>.
 */
class ParsingHalted : public std::exception 
{
public:
    virtual const char *what() const noexcept 
    { 
        return "parsing halted"; 
    }
};
    



/**
 * Klasa reprezentująca program w języku. Potrafi (docelowo) odczytywać pliki z programami, tworzyć na ich podstawie dzrwea składniowe oraz wykonywać je.
 * Generalnie za jej pomoca powinna odbywać się zała interakcja z językiem.
 *
 * \b Rejestry – zmienne są trzymane w rejestrach. Liczba rejestrów jest równa maksymalnej liczbie potrzebnych kontekstów w programie – jeden rejestr to jeden kontekst.
 * W przypadku wywołań rekurencyjnych, kiedy powstaje nowy rejestr (kontekst) o tym samym numerze, stary jest odkładany na stosie, a potem przywracany.
 */
class Program
{
protected:
    ///W przypadku rzucenia wyjątku <c>ParsingHalted</c> tutaj zapisane sostaną informacje o błędzie
    ParseInfo _parseInfo;
    
    ///Intuicyjnie – maksymalna głębokość wcięcia; praktycznie – potrzebna liczba rejestrów; z definicji – największa głębokość rekursji funkcji <c>parseBlock</c>
    unsigned int _maxBlockDepth = 0;

    ///Obecnie zaalokowana liczba rejestrów
    unsigned int _registerSize = 0;

    ///Zarządca zmiennych globalnych
    VariablesManager *_globals = nullptr;

    ///Rejestry na zmienne. Jeden konstkst dopowiada jednemu rejestrowi (jeden <c>double*</c>), a każdy wpis w rejestrze to zmienna.
    AST::Variable **_register = nullptr;

    ///Stosy z przesłoniętymi rejestrami
    std::stack<AST::Variable*> *_registerSwap = nullptr;

    ///Wzkaźnik na używany obecnie do czytania obiekt <c>LexicalScanner</c>
    LexicalScaner *_scaner;

    /**
     * Funkcja odczytuje z pliku otwartego przez <c>_scaner</c> instrukcje aż do napotkania znacznika <c>endToken</c>. Zmienna <c>parentContext</c> opisuje kontekst
     * nadrzędny w stosunku do tego z bloku. Kiedy odczytywany blok jest zestawem globalnych instrukcji, należy ustawić <c>parentContext</c> na <c>nullptr</c>
     * oraz <c>endToken</c> na pusty napis.
     * \param parentContext obiekt klasy <c>VariablesManager</c> zawierający zmienne z nadrzędnego kontekstu (<c>nullptr</c> gdy brak)
     * \param endToken znacznik kończący blok
     * \throw ParsingHalted przy napotkaniu błędu kompilacji
     * \return Wskażnik na pierwszy węzeł drzewa składni ze sparsowanym blokiem
     * \bug Pewnie nie zwalnia pamięci przy błędzie
     */
    AST::Node *parseBlock(Interpreter::VariablesManager *parentContext, string endToken);

    /**
     * Funkcja parsuje deklarację zmiennych (z pominięciem słowa 'Decl'!). Zapisane w danej dekraracji zmienne są dodawane do obiektu <c>variables</c>
     * (nie może być <c>nullptr</c>).
     * \throw ParsingHalted przy napotkaniui błędu składni/jakiegoś-innego
     */
    void parseDeclaration(Interpreter::VariablesManager *variables);

    /**
     * Parsuje wyrażenie arytmetyczne zapisane w notacji infiksowej za pomocą zmiennych, operatorów oraz stałych liczbowych.
     * \param variables wskażnik (nie <c>nullptr</c>) na obiek zarządzający zmiennymi z bierzącego kontekstu
     * \param constType typ dla stałych w wyrażeniu
     * \return wstaźnik na zapisane w onp przy pomocy struktur dzrwea składniowego działanie
     */
    AST::RpnNode *parseExpression(Interpreter::VariablesManager *variables, AST::Variable::Type constType);

    /**
     * Wypełnia odpowiednio zmienną <c>_parseInfo</c> i zgłasza wyjątek <c>ParsingHalted</c>.
     * \param err Kod błędu do zgłoszenia
     * \param msg Komentarz na temat błędu
     */
    void raiseParseError(ParseInfo::Error err, const std::string &msg) {
        raiseParseError(err, msg.c_str());
    }

    /**
     * Wypełnia odpowiednio zmienną <c>_parseInfo</c> i zgłasza wyjątek <c>ParsingHalted</c>.
     * \param err Kod błędu do zgłoszenia
     * \param msg Komentarz na temat błędu
     */
    void raiseParseError(ParseInfo::Error err, const char *msg) {
        _parseInfo.line = _scaner->line();
        _parseInfo.column = _scaner->column();
        _parseInfo.error = err;
        _parseInfo.message = std::string(msg);
        throw ParsingHalted();
    }

    /**
     * Wykonuje instrukcje opisane w podanym drzewie.
     * \param ast drzewo składniowe z instrukcjami do wykonania
     */
    void exec(AST::Node *ast);


public:
    /**
     * Domyślny konstruktor.
     */
    Program();

    /**
     *A to jest destruktor.
     */
    ~Program();

    /**
     * Dodaje do programu plik żródłowy o podanej lokalizacji. Jest kompilowany do drzewa składniowego. Zdefiniowane funkcje i zmienne są zapamiętywane.
     * Kod do inicjalizacji jest wykonywany i usuwany z pamięci.
     */
    ParseInfo sourceFile(const char *path);

    /**
     * Tworzy tablice do przechowywania rejestrów zmiennych.
     */
    void createRegister();
};

}

#endif // INTERPRETER_PROGRAM_HPP

