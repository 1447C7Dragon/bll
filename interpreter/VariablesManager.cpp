#include "VariablesManager.hpp"


using namespace Interpreter;

VariablesManager::VariablesManager(Interpreter::VariablesManager *parent)
{
    if (parent == nullptr)
        return;
    
    _parent = parent;
    _registerNumber = _parent->_registerNumber + 1;
}


bool VariablesManager::hasVariable(const std::string &name) const
{
    if (_map.find(name) != _map.end())
        return true;
    
    if (_parent != nullptr)
        return _parent->hasVariable(name);
    else 
        return false;
}


void VariablesManager::declare(const std::string &name, AST::Variable::Type type)
{
    if (_map.find(name) != _map.end())
        throw VariableRedeclared();
    AST::VariableAddress vi = {_registerNumber, _counter++};
    Variable v = {vi, type};
    _skeleton.push_back(type);
    _map.emplace(name, v);
}


VariablesManager::Variable VariablesManager::variable(const std::string& name) const
{
    if (_map.find(name) != _map.end())
        return _map.at(name);
    
    if (_parent != nullptr)
        return _parent->variable(name);
    else 
        throw VariableNotFound();
}


