#ifndef LEXICALSCANER_HPP
#define LEXICALSCANER_HPP
/** \file
 * Plik zawiera deklarację klasy <c>Interpreter::LexicalScaner</c>.
 */

#include <sstream>
#include <fstream>
#include <string>
#include <cctype>
#include <exception>

using namespace std;


namespace Interpreter {

/**
 * Obiekt służący do czytania znaczników ze strumienia z kodem źródłowym. Znaki są grupowane w znaczniki na podstawie typu
 * i zwracane w postaci znacznika. <c>LexicalScaner</c> utrzymuje w swoich zmiennych numer bierzącej linii oraz kolumny.
 */
class LexicalScaner
{
protected:
    ///Gdy jest ustawione na <c>true</c> wszystkie litery odczytywane z pliku są zamieniane na małe litery
    bool _caseSensitive;
    
    ///Numer bierzącej linii w pliku
    int _line;
    
    ///Numer bierzącej kolumny w pliku
    int _column;
    
    ///Nazwa czytanego pliku
    std::string _fileName;
    
    ///Jeśli jest <c>true</c> to czytanie dodarło do końca pliku
    bool _eof;
    
    ///Strumień do czytania znaków
    std::istream *_stream = nullptr;
    
    ///Ostatni odczytany znacznik
    ///\see token
    ///\see name
    ///\see number
    ///\see alnum
    std::string _token;
    
    /**
     * Odczytuje kolejny znak z pliku <c>_stream</c>. Wczytanie dowolnego znaku powoduje zwiększenie zmiennej <c>_column</c> o 1.
     * W przypadku, kiedy odczytany znak to <c>\\n</c>, to wówczas zmienna <c>_line</c> jest zwiększana o 1, a zmienna <c>_column</c>
     * ustawiana na 0. Przy próbie odczytania znaku po dotarciu do końca pliku zwracany jest <c>(char) 0</c>.
     * \return <c>_stream->get()</c> jeśli nie osiągnięto końca pliku, a w przeciwnym wypadku <c>(char) 0</c>
     */
    char nextChar();
    
public:
    /**
     * Konstruktor klasy <c>LexicalScaner</c>. Pozwala na ustawienie zmiennej <c>_caseSensitive</c>.
     * \param caseSensitive wartość do przypisania <c>_caseSensitive</c>
     */
    explicit LexicalScaner(bool caseSensitive = true);
    
    /**
     * Destruktor klasy LexicalScaner.
     */
    ~LexicalScaner();
    
    /**
     * Otwiera plik <c>fileName</c>. Zwraca true, kiedy został otwarty poprawnie.
     * \return <c>_stream->good()</c> po jego otwarciu
     */
    bool open(const char *fileName);
    
    /**
     * Odczytuje z pliku kolejny znacznik i wpisuje go do zmiennej <c>_token</c>. Zwraca <c>true</c> wtedy i tylko wtedy, kiedy
     * wartość wpisana do zmiennej <c>_token</c> jest różna od <c>""</c>. Jeżli został odczytany token zawierający spacje (złamania 
     * linii itp), to funkcja wywołuje się rekurencyjnie, zatem odczytywane sa tylko znaczniki nie będące spacjami.
     * \return Wartośc wyrażenia <c>_token != ""</c> na końcu funkcji.
     */
    bool nextToken();
    
    /**
     * Zwraca wartość zmiennej <c>_token</c>.
     * \return Wartość zmiennej <c>_token</c>
     */
    std::string token();
    
    /**
     * Zwraca wartość zmiennej <c>_token</c>, gdy jest ona poprawną nazwą alfanumerycznym, albo pusty napis.
     * \return Wartość zmiennej <c>_token</c> wtedy i tylko wtedy, gdy <c>isName(_token)</c>. W przeciwnym wypadku zwraca <c>""</c>
     * \see isName
     */
    std::string name();
    
    /**
     * Zwraca wartość zmiennej <c>_token</c>, gdy jest ona napisem liczbą, albo pusty napis.
     * \return Wartość zmiennej <c>_token</c> wtedy i tylko wtedy, gdy <c>isNumber(_token)</c>. W przeciwnym wypadku zwraca <c>""</c>
     * \see isNumber
     */
    std::string number();
    
    /**
     * Zwraca wartość zmiennej <c>_token</c>, gdy jest ona napisem alfanumerycznym, albo pusty napis.
     * \return Wartość zmiennej <c>_token</c> wtedy i tylko wtedy, gdy <c>isAlnum(_token)</c>. W przeciwnym wypadku zwraca <c>""</c>
     * \see isAlnum(str)
     */
    std::string alnum();
    
    /**
     * Porównuje parametr <c>str</c> ze zmienną <c>_token</c>. Jeżeli są równe, wywołuje funkcję <c>nextToken</c> zwraca <c>true</c>.
     * W innym wypadku zwraca <c>false</c>.
     * \param str napis do dopasowania
     * \return <c>_token == str</c>
     */
    bool match(std::string str);
    
    /**
     * Porównuje parametr <c>str</c> ze zmienną <c>_token</c>. Jeżeli są równe, wywołuje funkcję <c>nextToken</c> zwraca <c>true</c>.
     * W innym wypadku zwraca <c>false</c>.
     * \param str napis do dopasowania
     * \return <c>_token == str</c>
     */
    bool match(const char *str);
    
    /**
     * Sprawdza czy podany napis jest poprawną nazwą dla zmiennej, funkcji itp. Poprawna nazwa: napis złożony z liter ascii, cyfr oraz
     * podkreślnika, którego pierwszym znakiem jest litera.
     * \param str napis do sprawdzenia
     * \return <c>true</c> wtedy i tylko wtedy, gdy napis jest poprawną nazwą.
     */
    static bool isName(const std::string &str);

    /**
     * Sprawdza czy podany napis jest poprawną liczbą (z lub bez kropki). Poprawna liczba składa się z cyfr 0-9 oraz
     * maksymalnie jednej kropki.
     * \param str napis do sprawdzenia
     * \return <c>true</c> wtedy i tylko wtedy, gdy napis jest poprawną liczbą.
     */
    static bool isNumber(const std::string &str);

    /**
     * Sprawdza czy podany napis jest alfanumeryczny.
     * \param str napis do sprawdzenia
     * \return <c>true</c> wtedy i tylko wtedy, gdy napis jest alfanumeryczny.
     */
    static bool isAlnum(const std::string &str);
    
    /**
     * Zwraca numer linii w otwartym pliku, z której odczytano ostatni znacznik.
     * \return numer bierzącej linii
     */
    int line() const {
        return _line;
    }
    
    /**
     * Zwraca numer znaku ostatnio przeczytanego znaku w bierzącej linii.
     * \return numer bierzącej kolumny
     */
    int column() const {
        return _column;
    }
    
    /**
     * Jeżeli zwróci true, to znaczy że osiągnięto koniec pliku.
     * \return <c>true</c>, gdy nie udało się odczytać ostatniego znacznika z powodu końca pliku.
     */
    bool eof() {
        return _eof;
    }
};

}

#endif // LEXICALSCANER_H
