#ifndef VARIABLESMANAGER_H
#define VARIABLESMANAGER_H
/** \file
 * Plik zawiera deklarację klasy <c>VariablesManager</c> oraz powiązane z nim klasy wyjątków.
 */

#include <unordered_map>
#include <string>
#include <exception>
#include <vector>

#include "AstStructures.hpp"

namespace Interpreter
{

/**
 * Wyjątek rzucany w wypadku nieistnienia zmiennej o podanej nazwie, kiedy zarządano podania jej adresu.
 */
class VariableNotFound : std::exception
{
public:
    virtual const char *what() const noexcept {
        return "variable not found";
    }
};

/**
 * Wyjątek rzucany w momencia rządania deklaracji zmiennej o takiej samej nazwie jak już istniejąca w tej instancji obiektu.
 */
class VariableRedeclared : std::exception
{
public:
    virtual const char *what() const noexcept {
        return "variable redeclared";
    }
};

/**
 * Klasa służąca do zarządzania adresami zmiennyw w trakcie kompilacji pliku. Ma przyporządkowany numer rejestru,
 * w którym mają zostac umieszczone kolejne dodawane. W momencie dodania, zmiennej przypisywany jest kolejny
 * numer w rejestrze, począwszy od zera – indeksy w tablicy będącej rejestrem. Pojedynczy obiekt kontroluje jeden
 * rejestr, następny względem rejestru jego rodzica (rejestr zerowy, gdy do inicjalizacji obiektu został podany
 * wskaźnik <c>nullptr</c>).
 *
 * Do dodanej zmiennej można się potem odwołać podając jej nazwę. Jeżeli zmienna o tej nazwie istnieje, to zwracany
 * jest jej adres w rejestrze. W przeciwnym wypadku zapytanie jest przekierowane do rodzica (jeśli istnieje).
 * Zmienne z kontekstów-rodziców mogą być.
 */
class VariablesManager
{
public:
    struct Variable
    {
        AST::VariableAddress addr;
        AST::Variable::Type type;
    };


protected:
    ///Wskaźnik na rodzica. Rodzic to zarządce zmiennych kontekstu, w którym jest zagnieżdżony ten reprezentowany
    ///przez obiekt. <c>nullptr</c> gdy rodzic nie istnieje.
    VariablesManager *_parent = nullptr;

    ///Numer rejestru (głębokośc zagnieżdżenia kontekstu) tego obiektu
    unsigned int _registerNumber = 0;

    ///Licznik dodanych zmiennych
    unsigned int _counter = 0;

    ///Mapa nazw zmiennych na informacje o nich
    std::unordered_map<std::string, Variable> _map;

    ///typy zmiennych w kolejności występowania w rejestrze (szkielet rejestru)
    std::vector<AST::Variable::Type> _skeleton;

public:
    /**
     * Konstruktor obiektu. Możliwe są dwie sytuacje:
     *  - gdy <c>parent != nullptr</c>: kontekst reprezentowany tworzonym obiektem jest zagnieżdżony w tym reprezentowanym
     *    przez zmienną <c>parent</c>. Wowczas numer rejestru tego obiektu będzie równy numerowoi rejestru rodzica powiększonemu
     *    o 1.
     *  - gdy <c>parent == nullptr</c>: kontekst nie jest zagnieżdżony w żadnym innym (deklarowane w nim zmienne są globalne).
     *    Wówczas numer rejestru tego obiektu to 0.
     */
    VariablesManager(VariablesManager *parent);

    /**
     * Sprawdza czy w tym kontekście lub w którymkolwiek z kontekstów nadrzędnych znajduje się zmienna o podanej nazwie.
     * \param name nazwa zmiennej do wyszukania
     * \return <c>true</c> wtedy i tylko wtedy, gdy zmienna o podanej nazwie znajduje się w tym kontekście lub którymś
     * z nadrzędnych
     */
    bool hasVariable(const std::string &name) const;

    /**
     * Zwraca informacje o zmiennej o podanej nazwie. W tym celu przeszukiwany jest najpierw ten obiekt, a następnie kolejne
     * obiekty-rodzice, aż do momenyu znalezienia zmiennej o podanej nazwie. W wypadku, kiedy zmienna nie zostanie
     * odnaleziona rzucony zostanie wyjątek.
     * \param name nazwa zmiennej do wyszukania
     * \return adres zmiennej o podanej nazwie
     * \throw Interpreter::VariableNotFound gdy zmienna nie zostanie odnaleziona
     */
    Variable variable(const std::string &name) const;

    /**
     * Tworzy w tym obiekcie nową zmienną i przypisuje jej pierwszy niewykorzystany adres. Możeliwe jest przesłanianie
     * zmiennych z kontekstów nadrzędnych – wówczas zmienna jest zwyczajnie tworzona w bierzącym kontekście, a ponieważ szukanie
     * zmiennych rozpoczyna się od tego obiektu i kończy w momencie znalezienia zmiennej, to zmienne z kontekstów nadrzędnych
     * zostaną istotnie przesłonięte. W wypadku kiedy zmianna o tej nazwie została już dodana do tego konkretni obiektu
     * zostanie zgłoszony wyjątek.
     * \param name nazwa dla tworzonej zmiennej
     * \param type typ zmiennej
     * \throw Interpreter::VariableRedeclared kiedy zmienna o tej nazwie już została zadeklarowana w tym obiekcie
     */
    void declare(const std::string &name, AST::Variable::Type type);

    /**
     * Zwraca numer rejestru skojarzony z tym obiektem.
     * \return numer rejestru skojarzony z tym obiektem.
     */
    unsigned int registerNumber() {
        return _registerNumber;
    }

    /**
     * Zwraza liczbę zmiennych dodanych do tego obiektu.
     * \return liczbę zmiennych w obiekcie
     */
    unsigned int size() {
        return _counter;
    }

    /**
     * Zwraca zmienną ze szkieletu rejestru (Sounds mysteriously, doesn't it?).
     * \param num numer zmiennej w rejestrze
     * \return Informacje o zmiennej, która będzie znajdować się w rejestrze na podanej pozycji
     */
    AST::Variable::Type skeleton(unsigned int num) {
        return _skeleton[num];
    }

};

}

#endif // VARIABLESMANAGER_H


