#include "LexicalScaner.hpp"
#include <iostream>


using namespace Interpreter;

LexicalScaner::LexicalScaner(bool caseSensitive)
{
    _caseSensitive = caseSensitive;
}


bool LexicalScaner::open(const char *fileName)
{
    _eof = false;
    _stream = new std::fstream(fileName);
    if (!_stream->good()) {
        delete _stream;
        _stream = nullptr;
        return false;
    }
    _fileName = fileName;
    _line = 1;
    _column = 0;
    nextToken();
    return true;
}


LexicalScaner::~LexicalScaner()
{
    delete _stream;
}



char LexicalScaner::nextChar()
{
    if (_stream == nullptr || _stream->eof())
        return '\0';

    static int ch = 0;
    if (ch == '\n') {
        ++_line;
        _column = 0;
    }

    ch = _stream->get();
    ++_column;
    if (ch == '#') {
        while (ch != '\n')
            ch = _stream->get();
    }

    if (ch == char_traits<char>::eof()) {
        ch = 0;
        _eof = true;
    }
    if (_caseSensitive)
        return char(ch);
    return tolower(char(ch));
}


bool LexicalScaner::nextToken()
{
    static char readChar = nextChar();
    enum Type : unsigned char {
        SPACE,
        ALNUM,
        OPERATOR,   
        BRECKET,
        UNKNOWN,
        END_STATMENT,
        NONE
    };
    
    auto char_type = [](char ch) {
        if (isalnum(ch))
            ch = 'a';
        switch (ch) {
            case ' ':
            case '\t':
                return SPACE;
            case '\n':
            case ';':
                return END_STATMENT;
            case 'a':
                return ALNUM;
            case '+':
            case '-':
            case '*':
            case '/':
            case '%':
            case ',':
            case '=':
            case '<':
            case '>':
            case '!':
                return OPERATOR;
            case '(':
            case ')':
            case '[':
            case ']':
                return BRECKET;
            default:
                return UNKNOWN;
        }
    };
    
    
    Type prevType, type = NONE;
    _token = "";
    while (!_stream->eof()) {
        prevType = type;
        type = char_type(readChar);
        
        if (prevType != NONE && prevType != type)
            break;
        if (type != SPACE)
            _token += readChar;
        readChar = nextChar();
        if (type == BRECKET)
            break;
    }
    
    if (readChar == 0) {
        _eof = true;
        return false;
    }
    if (prevType == END_STATMENT)
        _token = ";";
    if (prevType == SPACE)
        return nextToken();
    return true;
}


string LexicalScaner::token()
{
    return _token;
}


bool LexicalScaner::match(const char *str)
{
    if (_token != str)
        return false;
    nextToken();
    return true;
}


bool LexicalScaner::match(string str)
{
    return match(str.c_str());
}


bool LexicalScaner::isName(const std::string &str)
{
    if (!isalpha(str[0]))
        return false;
    for (char c : str) {
        if (!isalnum(c) && c != '_')
            return false;
    }
    return true;
}


bool LexicalScaner::isNumber(const std::string &str)
{
    bool dot = false;
    for (char c : str) {
        if (c == '.') {
            if (dot)
                return false;
            dot = true;
        } else if (!isdigit(c)) {
            return false;
        }
    }
    return true;
}


bool LexicalScaner::isAlnum(const std::string &str)
{
    for (char c : str) {
        if (!isalnum(c) && c != '_')
            return false;
    }
    return true;
}



string LexicalScaner::name()
{
    std::string tmp;
    if (isName(_token)) {
        tmp = _token;
        nextToken();
    }
    return tmp;
}


string LexicalScaner::number()
{
    std::string tmp;
    if (isNumber(_token)) {
        tmp = _token;
        nextToken();
    }
    return tmp;
}


string LexicalScaner::alnum()
{
    std::string tmp;
    if (isAlnum(_token)) {
        tmp = _token;
        nextToken();
    }
    return tmp;
}



