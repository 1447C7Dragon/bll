#include "AstStructures.hpp"
#include "VariablesManager.hpp"

void Interpreter::AST::rpn_delete(Interpreter::AST::RpnNode *rpn)
{
    unsigned int length = 0;
    for (AST::RpnNode *block = rpn; block!= nullptr; block = block->next) 
        ++length;
    for (; length > 0; --length) {
        AST::RpnNode *next = rpn->next;
        switch (rpn->op) {
        case Operator::PUSH_VAL:
            delete static_cast<double*>(rpn->arg);
            break;
        case Operator::PUSH_VAR:
            delete static_cast<VariableAddress*>(rpn->arg);
            break;
        }
        delete rpn;
        rpn = next;
    }
}


void Interpreter::AST::ast_delete(Interpreter::AST::Node *ast)
{
    while (ast != nullptr) {
        Node *toDelete = ast;
        ast = ast->next;
        
        if (toDelete->data != nullptr) {
            switch (toDelete->inst) {
                case Node::PUSH_CONTEXT: {
                    PushContextData *data = static_cast<PushContextData*>(toDelete->data);
                    delete [] data->skel;
                    delete data;
                    break;
                }
                case Node::POP_CONTEXT:
                    delete static_cast<PushContextData*>(toDelete->data);
                    break;
                case Node::ASSIGN: {
                    AssignementData *data = static_cast<AssignementData*>(toDelete->data);
                    rpn_delete(data->expr);
                    delete data;
                    break;
                }
                case Node::OUTPUT:
                    delete static_cast<VariableAddress*>(toDelete->data);
                    break;
            }
        }
        
        delete toDelete;
    }
}


