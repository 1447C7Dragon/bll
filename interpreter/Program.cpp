#include "Program.hpp"

using namespace Interpreter;

Program::Program()
{
    _globals = new VariablesManager(nullptr);
}


Program::~Program()
{
    delete _globals;
    for (int i = 0; i < _registerSize; ++i) {
        while (!_registerSwap[i].empty()) {
            AST::Variable *regrow = _registerSwap[i].top();
            _registerSwap[i].pop();
            delete [] regrow;
        }
    }
    delete [] _registerSwap;
    delete [] _register;
}


AST::Node *Program::parseBlock(VariablesManager *context, std::string endToken)
{
    if (context->registerNumber() + 1 > _maxBlockDepth)
        _maxBlockDepth = context->registerNumber() + 1;
    
    //INICJALIZACJA DRZEWA (wartości do pierwszej instukcji są wpisywane później)
    AST::Node *firstBlock = new AST::Node;
    firstBlock->inst = AST::Node::PUSH_CONTEXT;
    firstBlock->next = nullptr;
    AST::Node *lastBlock = firstBlock;
    
    //PARSOWANIE BLOKU
    while (!_scaner->match(endToken)) {
        if (_scaner->eof()) {
            if (endToken != "")
                raiseParseError(ParseInfo::UNEXPECTED_EOF, "file ended but block's end not found");
            else
                break;
        }
        
        AST::Node *node = nullptr;

        //PARSOWANIE DEKLARACJI ZMIENNEJ
        if (_scaner->match("decl")) {
            do {
                try {
                    parseDeclaration(context);
                } catch (ParsingHalted) {
                    AST::ast_delete(firstBlock);
                    throw ParsingHalted();
                }
                _scaner->match(",");
            } while (!_scaner->match(";") && !_scaner->eof());

        //PRASOWANIE INSTRUKCJI PRZERWANIA WYKONANIA
        } else  if (_scaner->match("halt")) {
            node = new AST::Node;
            node->inst = AST::Node::HALT;
            _scaner->match(";");

        //PARSOWANIE INSTRUKCJI WYPISANIA ZMIENNEJ
        } else if (_scaner->match("output")) {
            AST::VariableAddress *addr = new AST::VariableAddress;
            std::string name = _scaner->name();

            if (name == "")
                raiseParseError(ParseInfo::NAME_EXPECTED, "expected variable name");
            try {
                *addr = context->variable(name).addr;
            } catch (VariableNotFound) {
                delete addr;
                AST::ast_delete(firstBlock);
                raiseParseError(ParseInfo::UNKNOWN_NAME, std::string("variable '") + name + "' not found");
            }

            node = new AST::Node;
            node->inst = AST::Node::OUTPUT;
            node->data = addr;
            _scaner->match(";");

        //PARSOWANIE POZOSTAŁÝCH INSTRUKCJI (NIE SIĘ ICH ROZPOZNAC NA PODSTAWIE PIERWSZEGO SYMBOLU
        } else {
            std::string name = _scaner->name();
            if (name == "") {
                AST::ast_delete(firstBlock);
                raiseParseError(ParseInfo::UNEXPECTED_TOKEN, std::string("Unrecongized instruction '") + _scaner->token() + "'");
            }

            //PRZYPISANIE
            if (_scaner->match("=")) {
                AST::VariableAddress addr;
                try {
                    addr = context->variable(name).addr;
                } catch (VariableNotFound) {
                    AST::ast_delete(firstBlock);
                    raiseParseError(ParseInfo::UNKNOWN_NAME, std::string("No variable called '") + name + "' in current context");
                }
                AST::RpnNode *rpn = parseExpression(context, context->variable(name).type);
                AST::AssignementData *data = new AST::AssignementData;
                data->address = addr;
                data->expr = rpn;
                node = new AST::Node;
                node->data = data;
                node->inst = AST::Node::ASSIGN;

            //NIEROZPOZNANO INSTRUKCJI
            } else {
                AST::ast_delete(firstBlock);
                raiseParseError(ParseInfo::UNEXPECTED_TOKEN, std::string("Unrecongised instruction"));
            }

            _scaner->match(";");
        }

        //DOPISYWANIE INSTRUKCJI NA KOŃCU DRZEWA
        if (node != nullptr) {
            lastBlock->next = node;
            lastBlock = node;
        }
    }
    
    //WPISYWANIE DANYCH DO PIERWSZEJ INSTRUKCJI (PUSH_CONTEXT)
    AST::PushContextData *pushData = new AST::PushContextData;
    pushData->regnum = context->registerNumber();
    pushData->size = context->size();
    pushData->skel = new AST::Variable::Type [context->size()];
    for (unsigned int i = 0; i < context->size(); ++i)
        pushData->skel[i] = context->skeleton(i);
    firstBlock->data = pushData;
    
    //TWORZENIE I DODAWANIE OSTATNIEJ INSTRUKCJI (POP_CONTEXT)
    AST::Node *popNode = new AST::Node;
    AST::PushContextData *popData = new AST::PushContextData;
    *popData = *pushData;
    popData->skel = nullptr;
    popNode->data = popData;
    popNode->inst = AST::Node::POP_CONTEXT;
    lastBlock->next = popNode;
    
    return firstBlock;
}


void Program::parseDeclaration(VariablesManager *variables)
{
    std::string name = _scaner->name();
    if (name == "")
        raiseParseError(ParseInfo::NAME_EXPECTED, "valid variable name expected");

    if (!_scaner->match("as"))
        raiseParseError(ParseInfo::UNEXPECTED_TOKEN, "expected 'as'");

    AST::Variable::Type type;
    if (_scaner->match("byte")) {
        type = AST::Variable::BYTE;
    } else if (_scaner->match("integer")) {
        type = AST::Variable::INTEGER;
    } else if (_scaner->match("double")) {
        type = AST::Variable::DOUBLE;
    } else {
        raiseParseError(ParseInfo::UNKNOWN_NAME, "expected type identifier");
    }

    try {
        variables->declare(name, type);
    } catch (VariableRedeclared) {
        raiseParseError(ParseInfo::NAME_EXPECTED, "variable redeclared");
    }
}


AST::RpnNode *Program::parseExpression(VariablesManager *variables, AST::Variable::Type constType)
{
    AST::RpnNode *firstNode = nullptr, *lastNode;
    auto pushNode = [&](AST::RpnNode *node) {
        if (firstNode == nullptr) {
            firstNode = node;
            lastNode = firstNode;
        } else {
            lastNode->next = node;
            lastNode = node;
        }
    };

    auto parseName = [&](const std::string &name) {
        AST::RpnNode *node = new AST::RpnNode;
        if (isalpha(name[0])) {
            AST::VariableAddress *addr = new AST::VariableAddress;
            try {
                *addr = variables->variable(name).addr;
            } catch (VariableNotFound) {
                delete addr;
                raiseParseError(ParseInfo::UNKNOWN_NAME, std::string("variable '") + name + "' not found");
            }
            node->op = AST::Operator::PUSH_VAR;
            node->arg = addr;
        } else {
            if (!LexicalScaner::isNumber(name))
                raiseParseError(ParseInfo::UNKNOWN_NAME, std::string("string '") + name + "' doesn't seem to be a number...");
            switch (constType) {
                case AST::Variable::BYTE: {
                    AST::Variable::TByte *val = new AST::Variable::TByte;
                    *val = stoi(name);
                    node->arg = val;
                    break;
                }
                case AST::Variable::INTEGER: {
                    AST::Variable::TInteger *val = new AST::Variable::TInteger;
                    *val = stoi(name);
                    node->arg = val;
                    break;
                }
                case AST::Variable::DOUBLE: {
                    AST::Variable::TDouble *val = new AST::Variable::TDouble;
                    *val = stoi(name);
                    node->arg = val;
                    break;
                }
            }
            node->op = AST::Operator::PUSH_VAL;
        }
        pushNode(node);
    };

    std::string op;
    auto parseFactor = [&]() {
        parseName(_scaner->alnum());
        op = _scaner->token();
        while (op == "/" || op == "*") {
            _scaner->nextToken();
            std::string token = _scaner->token();
            if (token == "(") {
                _scaner->nextToken();
                pushNode(parseExpression(variables, constType));
                for (; lastNode->next != nullptr; lastNode = lastNode->next) {}
                if (!_scaner->match(")"))
                    raiseParseError(ParseInfo::UNEXPECTED_TOKEN, "expected ')'");
            } else {
                parseName(_scaner->alnum());
            }

            AST::RpnNode *node = new AST::RpnNode;
            if (op == "/")
                node->op = AST::Operator::DIVIDE;
            else if (op == "*")
                node->op = AST::Operator::MULTIPLY;
            pushNode(node);

            op = _scaner->token();
        }
    };

    parseFactor();
    while (op == "+" || op == "-") {
        _scaner->nextToken();
        std::string oop = op;
        parseFactor();
        AST::RpnNode *node = new AST::RpnNode;
        if (oop == "+")
            node->op = AST::Operator::ADD;
        else
            node->op = AST::Operator::SUBTRACT;
        pushNode(node);
    }

    return firstNode;
}


ParseInfo Program::sourceFile(const char *path)
{
    _scaner = new LexicalScaner(false);
    if (!_scaner->open(path)) {
        _parseInfo.line = -1;
        _parseInfo.column = -1;
        _parseInfo.error = ParseInfo::IO_ERROR;
        _parseInfo.message = std::string("cannot open file '") + path + "'";
        return _parseInfo;
    }
    _parseInfo.file = std::string(path);
    
    try {
        AST::Node *initializer = parseBlock(_globals, "");
        exec(initializer);
        AST::ast_delete(initializer);
    } catch (ParsingHalted) {
        std::cerr << "Failed to source file '" << path << "': " << _parseInfo.message << std::endl;
    } 
    
    delete _scaner;
    _scaner = nullptr;
    return _parseInfo;
}


void Program::createRegister()
{
    if (_registerSize >= _maxBlockDepth)
        return;

    for (int i = 0; i < _registerSize; ++i) {
        while (!_registerSwap[i].empty()) {
            AST::Variable *regrow = _registerSwap[i].top();
            _registerSwap[i].pop();
            delete [] regrow;
        }
    }
    delete [] _registerSwap;
    delete [] _register;

    _register = new AST::Variable* [_maxBlockDepth];
    _registerSwap = new std::stack<AST::Variable*> [_maxBlockDepth];
    for (int i = 0; i < _maxBlockDepth; ++i)
        _register[i] = nullptr;
}


void Program::exec(AST::Node *ast)
{
    createRegister();
    while (ast != nullptr) {
        switch (ast->inst) {

            case AST::Node::HALT: {
                return;
                break;
            }

            case AST::Node::PUSH_CONTEXT: {
                AST::PushContextData *data = static_cast<AST::PushContextData*>(ast->data);
                if (_register[data->regnum] != nullptr)
                    _registerSwap[data->regnum].emplace(_register[data->regnum]);
                _register[data->regnum] = new AST::Variable [data->size];
                for (unsigned int i = 0; i < data->size; ++i) {
                    _register[data->regnum][i].type = data->skel[i];
                    switch (data->skel[i]) {
                        case AST::Variable::BYTE:
                            _register[data->regnum][i].ptr = new AST::Variable::TByte;
                            break;
                        case AST::Variable::INTEGER:
                            _register[data->regnum][i].ptr = new AST::Variable::TInteger;
                            break;
                        case AST::Variable::DOUBLE:
                            _register[data->regnum][i].ptr = new AST::Variable::TDouble;
                            break;
                    };
                }
                ast = ast->next;
                break;
            }

            case AST::Node::POP_CONTEXT: {
                AST::PushContextData *data = static_cast<AST::PushContextData*>(ast->data);
                for (unsigned int i = 0; i < data->size; ++i) {
                    switch (_register[data->regnum][i].type) {
                        case AST::Variable::BYTE:
                            delete static_cast<AST::Variable::TByte*>(_register[data->regnum][i].ptr);
                            break;
                        case AST::Variable::INTEGER:
                            delete static_cast<AST::Variable::TInteger*>(_register[data->regnum][i].ptr);
                            break;
                        case AST::Variable::DOUBLE:
                            delete static_cast<AST::Variable::TDouble*>(_register[data->regnum][i].ptr);
                            break;
                    };
                }
                delete [] _register[data->regnum];
                _register[data->regnum] = nullptr;
                if (!_registerSwap[data->regnum].empty()) {
                    _register[data->regnum] = _registerSwap[data->regnum].top();
                    _registerSwap[data->regnum].pop();
                }
                ast = ast->next;
                break;
            }

            case AST::Node::ASSIGN: {
                AST::AssignementData *data = static_cast<AST::AssignementData*>(ast->data);
                switch (_register[data->address.regnum][data->address.num].type) {
                    case AST::Variable::BYTE:
                        *static_cast<AST::Variable::TByte*>(_register[data->address.regnum][data->address.num].ptr) = AST::rpn_value<AST::Variable::TByte>(data->expr, const_cast<const AST::Variable**>(_register));
                        break;
                    case AST::Variable::INTEGER:
                        *static_cast<AST::Variable::TInteger*>(_register[data->address.regnum][data->address.num].ptr) = AST::rpn_value<AST::Variable::TInteger>(data->expr, const_cast<const AST::Variable**>(_register));
                        break;
                    case AST::Variable::DOUBLE:
                        *static_cast<AST::Variable::TDouble*>(_register[data->address.regnum][data->address.num].ptr) = AST::rpn_value<AST::Variable::TDouble>(data->expr, const_cast<const AST::Variable**>(_register));
                        break;
                }
                ast = ast->next;
                break;
            }

            case AST::Node::OUTPUT: {
                AST::VariableAddress *data = static_cast<AST::VariableAddress*>(ast->data);
                switch (_register[data->regnum][data->num].type) {
                    case AST::Variable::BYTE:
                        std::cout << "TByte@(" << data->regnum << ":" << data->num << ") = " << static_cast<unsigned int>(*static_cast<AST::Variable::TByte*>(_register[data->regnum][data->num].ptr)) << std::endl;
                        break;
                    case AST::Variable::INTEGER:
                        std::cout << "TInteger@(" << data->regnum << ":" << data->num << ") = " << *static_cast<AST::Variable::TInteger*>(_register[data->regnum][data->num].ptr) << std::endl;
                        break;
                    case AST::Variable::DOUBLE:
                        std::cout << "TDouble@(" << data->regnum << ":" << data->num << ") = " << *static_cast<AST::Variable::TDouble*>(_register[data->regnum][data->num].ptr) << std::endl;
                        break;
                }
                ast = ast->next;
                break;
            }

        }
    }
}
