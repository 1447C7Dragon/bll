/** \file
 * Plik z funkcją <c>main()</c>
 */
#include <iostream>
#include <string>
#include <chrono>

#include "interpreter/Program.hpp"

int main(int argc, char **argv) {
    Interpreter::Program prg;
    std::cout << "Sourcing 'a.test'..." << std::endl;
    
    auto start = std::chrono::system_clock::now();
    Interpreter::ParseInfo result = prg.sourceFile("a.test");
    auto end = std::chrono::system_clock::now();
    std::cout << "Sourcing finished in " << static_cast<chrono::duration<double>>(end - start).count() << "s." << std::endl;
    
    if (result.error != Interpreter::ParseInfo::NO_ERROR) {
        std::cout << "Failed: " << result.message << " (code " << static_cast<int>(result.error) << ") "
                  << "at " << result.file << ":" << result.line << ":" << result.column << std::endl;
        return 1;
    }   
    std::cout << "No errors found." << std::endl;
    return 0;
}
